fasta3 (36.3.8i.14-Nov-2020-3) unstable; urgency=medium

  * Team upload.
  * d/tests/control: the tests don't need python3-mysql.connector (which is
    not available in Debian Testing)

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 28 Oct 2024 10:34:11 +0100

fasta3 (36.3.8i.14-Nov-2020-2) unstable; urgency=medium

  * Team upload.

  [ Michael R. Crusoe ]
  * Force use of packaged SIMDe library
  * Mark Debian-specific patches as not needing forwarding. Update my
    email address
  * Adjusted patch so that the autopkgtests run. Added bedtools as a
    test dependency.

  [ Andreas Tille ]
  * Cleanup after testing
    Closes: #1044536
  * Add python3-mysql.connector, python3-requests to Suggests and Test-Depends

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 25 Oct 2024 16:36:05 +0200

fasta3 (36.3.8i.14-Nov-2020-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Recommends: r-base-core

 -- Andreas Tille <tille@debian.org>  Sun, 25 Dec 2022 19:00:56 +0100

fasta3 (36.3.8h.2020-02-11-5) unstable; urgency=medium

  * Team upload.
  * Fix watch file
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 03 Nov 2022 13:45:04 +0100

fasta3 (36.3.8h.2020-02-11-4) unstable; urgency=medium

  [ Michael R. Crusoe ]
  * Team upload.
  * Install scripts, matrics, and misc scripts in
     /usr/share/fasta3/{scripts,data,misc}
  * Install all the example SQL scripts and data

  [ Steffen Moeller ]
  * Fix watchfile to detect new versions on github (routine-update)

  [ Andreas Tille ]
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Depends: python3

 -- Andreas Tille <tille@debian.org>  Thu, 09 Sep 2021 09:29:56 +0200

fasta3 (36.3.8h.2020-02-11-3) unstable; urgency=medium

  * Team upload.
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * debian/upstream/metadata: added Repository{,-Browse}
  * Enable buildiing on non-X86 via SIMD Everywhere library
  * fasta3-doc: Multi-Arch: foreign
  * Enable cross building
  * Improve hardening
  * drop unneeded build-dep on zlib1g-dev
  * Add autopkgtests
  * Install scripts/ to usr/share/fasta3/

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 18 Apr 2020 11:46:22 +0200

fasta3 (36.3.8h.2020-02-11-2) unstable; urgency=medium

  * Drop outdated README.source

 -- Andreas Tille <tille@debian.org>  Wed, 19 Feb 2020 18:15:29 +0100

fasta3 (36.3.8h.2020-02-11-1) unstable; urgency=medium

  * Team upload
  * New upstream version with free license
    Licenses was changed from Academics-only to BSD-2-clause
  * Point watch file to Github
  * Standards-Version: 4.5.0 (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Andreas Tille <tille@debian.org>  Tue, 11 Feb 2020 18:30:49 +0100

fasta3 (36.3.8h-2) unstable; urgency=medium

  * Team upload.
  * Breaks+Replaces: fasta3 (<< 36.3.8h)
    Closes: #948597
  * Remove trailing whitespace in debian/copyright
  * Remove trailing whitespace in debian/rules
  * Trim trailing whitespace.
  * Use secure URI in Homepage field.
  * Fix regexp in d/copyright
  * Package fasta3-doc in section non-free/doc

 -- Andreas Tille <tille@debian.org>  Sat, 11 Jan 2020 06:55:15 +0100

fasta3 (36.3.8h-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.0

    TODO: Do we really need to use non-free smith waterman code?
          There is a free libssw.  Please contact upstream!

  [ Steffen Moeller]
  * Standards-Version: 4.4.1
  * Fixed patches - somehow one Makefile.common was removed but needed
    that led to a FTBFS.

 -- Steffen Moeller <moeller@debian.org>  Mon, 06 Jan 2020 12:20:34 +0100

fasta3 (36.3.8g-1) unstable; urgency=low

  [ Andreas Tille ]
  * Moved packaging from SVN to Git

  [ Steffen Moeller]
  * Initial release (Closes: #895740)

  * New upstream version.
  * Fixed remaining lintian error.
  * Bumped policy to 4.1.4.

  [ Alexandre Mestiashvili]
  * Add zlib1g-dev to Build-Depends
  * Apply cme fix dpkg, fixing indentation, secure url and patch descriptions

 -- Steffen Moeller <moeller@debian.org>  Sun, 15 Apr 2018 16:08:39 +0200
